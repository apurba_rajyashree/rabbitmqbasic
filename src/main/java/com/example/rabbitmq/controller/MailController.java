package com.example.rabbitmq.controller;

import com.example.rabbitmq.entities.Mail;
import com.example.rabbitmq.service.MailProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mail")
public class MailController {

    private final MailProducerService mailProducerService;

    @GetMapping
    public ResponseEntity<?> sendMail() {
        Mail mail = Mail.builder()
                .receiver("apurba101@yopmail.com")
                .body("Testing rabbit-mq mail sender").build();

        for (short i = 0; i < 5; i++) {
            mail.setId(i);
            mailProducerService.produceMail(mail);
        }

        return ResponseEntity.ok("Mail has been successfully sent to queue");
    }
}
