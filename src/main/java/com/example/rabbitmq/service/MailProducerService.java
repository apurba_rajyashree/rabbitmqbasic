package com.example.rabbitmq.service;

import com.example.rabbitmq.entities.Mail;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class MailProducerService {
    private static final String ROUTING_KEY = "mail.queue.key";
    private static final String EXCHANGE_NAME = "common.exchange";
    private final RabbitTemplate rabbitTemplate;

    public MailProducerService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void produceMail(Mail mail) {
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, ROUTING_KEY, mail);
        System.out.println("Mail - " + mail.getId() + ", has been sent to queue");
    }
}
