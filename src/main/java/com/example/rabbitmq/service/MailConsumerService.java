package com.example.rabbitmq.service;

import com.example.rabbitmq.config.RabbitMqConfig;
import com.example.rabbitmq.entities.Mail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MailConsumerService {

    @RabbitListener(queues = {RabbitMqConfig.QUEUE_NAME})
    public void consumeMail1(Mail mail) {
        log.info("mail received with sequence: " + mail.getId() + " consumeMail1");

    }
}
