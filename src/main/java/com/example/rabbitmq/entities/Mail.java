package com.example.rabbitmq.entities;

import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class Mail implements Serializable {
    private Short id;
    private String receiver;
    private String subject;
    private String body;
    private String template;
    private List<Object> attachments;
    private Map<String, Object> models;

}
